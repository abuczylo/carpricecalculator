package com.carPriceCalculator.core.exception;

public class ValidationException extends CoreException {

    public ValidationException(String errorCode, String message) {
        super(errorCode, message);
    }
}
