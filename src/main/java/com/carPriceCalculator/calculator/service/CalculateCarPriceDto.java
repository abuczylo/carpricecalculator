package com.carPriceCalculator.calculator.service;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CalculateCarPriceDto {
    private int year;
    private double engineCapacity;
    private double mileage;
    private int ownersCount;
    private boolean notCrashed;
    private String username;
}
