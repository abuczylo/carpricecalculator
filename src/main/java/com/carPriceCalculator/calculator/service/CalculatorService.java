package com.carPriceCalculator.calculator.service;

public interface CalculatorService {
    void calculateCarPrice(CalculateCarPriceDto calculateCarPriceDto) throws Exception;
}
