package com.carPriceCalculator.calculator.service;

import com.carPriceCalculator.cars.service.AddCarDto;
import com.carPriceCalculator.cars.service.CarService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import weka.classifiers.Classifier;
import weka.classifiers.functions.LinearRegression;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.experiment.InstanceQuery;

import java.util.ArrayList;

@AllArgsConstructor
@Service
public class CalculatorServiceImpl implements CalculatorService {

    private final CarService carService;

    @Override
    public void calculateCarPrice(CalculateCarPriceDto calculateCarPriceDto) throws Exception {
        InstanceQuery query = new InstanceQuery();
        query.setUsername("postgres");
        query.setPassword("postgres");
        query.setQuery("select year, not_crashed, engine_capacity, mileage, owners_count, price from cars");

        ArrayList<Attribute> attributes = new ArrayList<>();
        Attribute yearAttribute = new Attribute("yearFeature");
        Attribute engineCapacityAttribute = new Attribute("engineCapacityFeature");
        Attribute mileageAttribute = new Attribute("mileageFeature");
        Attribute ownersCountAttribute = new Attribute("ownersCountFeature");
        Attribute notCrashedAttribute = new Attribute("notCrashedFeature");
        Attribute priceAttribute = new Attribute("priceLabel");
        attributes.add(yearAttribute);
        attributes.add(engineCapacityAttribute);
        attributes.add(mileageAttribute);
        attributes.add(ownersCountAttribute);
        attributes.add(notCrashedAttribute);
        attributes.add(priceAttribute);

        Instances trainingDataset = new Instances("trainData", attributes, 5000);
        trainingDataset.setClassIndex(trainingDataset .numAttributes() - 1);

        carService.findAll().forEach(carDto -> {
            Instance instance = new DenseInstance(6);
            instance.setValue(yearAttribute, new Double(carDto.getYear()));
            instance.setValue(engineCapacityAttribute, carDto.getEngineCapacity());
            instance.setValue(mileageAttribute, carDto.getMileage());
            instance.setValue(ownersCountAttribute, new Double(carDto.getOwnersCount()));
            instance.setValue(notCrashedAttribute, getValueForNotCrashed(carDto.getNotCrashed()));
            instance.setValue(priceAttribute, carDto.getPrice());
            trainingDataset.add(instance);
        });

        Classifier targetFunction = new LinearRegression();
        targetFunction.buildClassifier(trainingDataset);

        Instances unlabeledInstances = new Instances("predictions", attributes, 1);
        unlabeledInstances.setClassIndex(trainingDataset.numAttributes() - 1);
        Instance unlabeled = new DenseInstance(5);
        unlabeled.setValue(yearAttribute, new Double(calculateCarPriceDto.getYear()));
        unlabeled.setValue(engineCapacityAttribute, calculateCarPriceDto.getEngineCapacity());
        unlabeled.setValue(mileageAttribute, calculateCarPriceDto.getMileage());
        unlabeled.setValue(ownersCountAttribute, new Double(calculateCarPriceDto.getOwnersCount()));
        unlabeled.setValue(notCrashedAttribute, getValueForNotCrashed(calculateCarPriceDto.isNotCrashed()));
        unlabeledInstances.add(unlabeled);

        double prediction  = targetFunction.classifyInstance(unlabeledInstances.get(0));

        AddCarDto addCarDto = AddCarDto.builder()
                .notCrashed(calculateCarPriceDto.isNotCrashed())
                .engineCapacity(calculateCarPriceDto.getEngineCapacity())
                .mileage(calculateCarPriceDto.getMileage())
                .ownersCount(calculateCarPriceDto.getOwnersCount())
                .username(calculateCarPriceDto.getUsername())
                .year(calculateCarPriceDto.getYear())
                .price(prediction)
                .build();
        carService.addCar(addCarDto);
    }

    private double getValueForNotCrashed(String notCrashed) {
        return notCrashed.equals("true") ? 1.0 : 0.0;
    }

    private double getValueForNotCrashed(boolean notCrashed) {
        return notCrashed ? 1.0 : 0.0;
    }
}
