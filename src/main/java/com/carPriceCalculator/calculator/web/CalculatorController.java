package com.carPriceCalculator.calculator.web;

import com.carPriceCalculator.calculator.service.CalculateCarPriceDto;
import com.carPriceCalculator.calculator.service.CalculatorService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@AllArgsConstructor
@Controller
@RequestMapping("calculator")
public class CalculatorController {

    private final CalculatorService calculatorService;

    @GetMapping("/calculateCarPrice")
    public String calculateCarPrice(CalculateCarPriceViewModel calculateCarPriceViewModel) {
        return "calculate-car-price";
    }

    @PostMapping("/calculateCarPrice")
    public String calculateCarPrice(@Valid CalculateCarPriceViewModel calculateCarPriceViewModel, BindingResult result, Model model) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        CalculateCarPriceDto calculateCarPriceDto = CalculateCarPriceDto.builder()
                .notCrashed(calculateCarPriceViewModel.isNotCrashed())
                .engineCapacity(calculateCarPriceViewModel.getEngineCapacity())
                .mileage(calculateCarPriceViewModel.getMileage())
                .ownersCount(calculateCarPriceViewModel.getOwnersCount())
                .username(auth.getName())
                .year(calculateCarPriceViewModel.getYear())
                .build();
        calculatorService.calculateCarPrice(calculateCarPriceDto);
        return "redirect:/cars/index";
    }
}
