package com.carPriceCalculator.users.service;

public interface UserService {
    UserDto createNewUser(String email, String password);
}
