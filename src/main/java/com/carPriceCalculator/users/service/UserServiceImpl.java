package com.carPriceCalculator.users.service;

import com.carPriceCalculator.roles.persistance.DefaultRole;
import com.carPriceCalculator.roles.persistance.Role;
import com.carPriceCalculator.roles.persistance.RoleRepository;
import com.carPriceCalculator.users.persistance.User;
import com.carPriceCalculator.users.persistance.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDto createNewUser(String email, String password) {
        Role role = roleRepository.findByName(DefaultRole.USER.getName());
        User user = User.builder()
                .email(email)
                .password(passwordEncoder.encode(password))
                .roles(new HashSet<>(Arrays.asList(role)))
                .build();
        userRepository.save(user);
        return UserDto.builder()
                .id(user.getId())
                .email(user.getEmail())
                .password(user.getPassword())
                .roles(user.getRoles().stream().map(r -> r.getName()).collect(Collectors.toSet()))
                .build();
    }
}
