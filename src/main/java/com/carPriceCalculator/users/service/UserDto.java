package com.carPriceCalculator.users.service;

import lombok.Builder;

import java.util.Set;
import java.util.UUID;

@Builder
public class UserDto {
    private UUID id;
    private String email;
    private String password;
    private Set<String> roles;
}
