package com.carPriceCalculator.users.persistance;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends PagingAndSortingRepository<User, UUID> {

    List<User> findAllByEmail(String email);
}
