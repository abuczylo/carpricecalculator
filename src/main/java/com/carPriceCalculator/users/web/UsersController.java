package com.carPriceCalculator.users.web;

import com.carPriceCalculator.users.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@AllArgsConstructor
@Controller
@RequestMapping("users")
public class UsersController {

    private final UserService userService;

    @GetMapping("/signup")
    public String showSignUpForm(AddUserViewModel addUserViewModel) {
        return "add-user";
    }

    @PostMapping("/adduser")
    public String addUser(@Valid AddUserViewModel addUserViewModel, BindingResult result, Model model) {
        userService.createNewUser(addUserViewModel.getEmail(), addUserViewModel.getPassword());
        return "redirect:/home";
    }
}
