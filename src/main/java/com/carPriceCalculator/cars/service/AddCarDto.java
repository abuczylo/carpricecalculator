package com.carPriceCalculator.cars.service;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AddCarDto {
    private int year;
    private double engineCapacity;
    private double mileage;
    private int ownersCount;
    private boolean notCrashed;
    private double price;
    private String username;
}
