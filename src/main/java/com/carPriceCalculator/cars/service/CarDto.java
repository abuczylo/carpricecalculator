package com.carPriceCalculator.cars.service;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Builder
@Data
public class CarDto {
    private UUID id;
    private int year;
    private double engineCapacity;
    private double mileage;
    private int ownersCount;
    private String notCrashed;
    private String username;
    private double price;
}
