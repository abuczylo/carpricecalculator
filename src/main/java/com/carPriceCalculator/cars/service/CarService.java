package com.carPriceCalculator.cars.service;

public interface CarService {
    Iterable<CarDto> findAll();
    void addCar(AddCarDto addCarDto);
    void addRandomCars(String username);
}
