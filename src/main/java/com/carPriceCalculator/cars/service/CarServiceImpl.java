package com.carPriceCalculator.cars.service;

import com.carPriceCalculator.cars.persistance.Car;
import com.carPriceCalculator.cars.persistance.CarRepository;
import com.carPriceCalculator.users.persistance.User;
import com.carPriceCalculator.users.persistance.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class CarServiceImpl implements CarService {

    private final UserRepository userRepository;
    private final CarRepository carRepository;

    @Override
    public Iterable<CarDto> findAll() {
        return StreamSupport.stream(carRepository.findAll(Sort.by(Sort.Direction.DESC, "creationDate")).spliterator(), false).map(car ->
                CarDto.builder()
                        .id(car.getId())
                        .notCrashed(isNotCrashed(car.isNotCrashed()))
                        .engineCapacity(car.getEngineCapacity())
                        .mileage(car.getMileage())
                        .ownersCount(car.getOwnersCount())
                        .username(car.getUser().getEmail())
                        .year(car.getYear())
                        .price(car.getPrice())
                        .build())
                .collect(Collectors.toList());
    }

    @Override
    public void addCar(AddCarDto addCarDto) {
        User user = userRepository.findAllByEmail(addCarDto.getUsername()).get(0);
        Car car = Car.builder()
                .id(UUID.randomUUID())
                .notCrashed(addCarDto.isNotCrashed())
                .engineCapacity(addCarDto.getEngineCapacity())
                .mileage(addCarDto.getMileage())
                .ownersCount(addCarDto.getOwnersCount())
                .user(user)
                .year(addCarDto.getYear())
                .price(addCarDto.getPrice())
                .creationDate(OffsetDateTime.now())
                .build();
        carRepository.save(car);
    }

    @Override
    public void addRandomCars(String username) {
        ArrayList<Car> cars = new ArrayList<>();
        User user = userRepository.findAllByEmail(username).get(0);
        Random rd = new Random();
        OffsetDateTime creationDate = OffsetDateTime.now();
        for (int i = 0; i < 15; i++) {
            Car car = Car.builder()
                    .id(UUID.randomUUID())
                    .notCrashed(rd.nextBoolean())
                    .engineCapacity(getRandomNumber(1.0, 5.0, 1))
                    .mileage(getRandomNumber(0, 500000, 2))
                    .ownersCount(getRandomNumber(1, 5, rd))
                    .user(user)
                    .year(getRandomNumber(1950, 2021, rd))
                    .price(getRandomNumber(1000.0, 1000000.0, 2))
                    .creationDate(creationDate)
                    .build();
            cars.add(car);
        }

        carRepository.saveAll(cars);
    }

    private String isNotCrashed(boolean isNotCrashed) {
        return isNotCrashed ? "true" : "false";
    }

    private static int getRandomNumber(int min, int max, Random random) {
        return random.nextInt(max - min) + min;
    }

    private static double getRandomNumber(double min, double max, int places) {
        double value = ((Math.random() * (max - min)) + min);
        return round(value, places);
    }

    private static double round(double value, int places) {
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
