package com.carPriceCalculator.cars.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddCarViewModel {
    @NotNull(message = "Year is required.")
    private int year;

    @NotNull(message = "EngineCapacity is required.")
    private double engineCapacity;

    @NotNull(message = "Mileage is required.")
    private double mileage;

    @NotNull(message = "OwnersCount is required.")
    private int ownersCount;

    @NotNull(message = "NotCrashed is required.")
    private boolean notCrashed;

    @NotNull(message = "Price is required.")
    private double price;
}
