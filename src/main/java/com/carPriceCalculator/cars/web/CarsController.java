package com.carPriceCalculator.cars.web;

import com.carPriceCalculator.cars.service.AddCarDto;
import com.carPriceCalculator.cars.service.CarService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@AllArgsConstructor
@Controller
@RequestMapping("cars")
public class CarsController {

    private final CarService carService;

    @GetMapping("/index")
    public String showIndex(Model model) {
        model.addAttribute("cars", carService.findAll());
        return "car-index";
    }

    @GetMapping("/addCar")
    public String showAddCarForm(AddCarViewModel addCarViewModel) {
        return "add-car";
    }

    @PostMapping("/addCar")
    public String addCar(@Valid AddCarViewModel addCarViewModel, BindingResult result, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AddCarDto addCarDto = AddCarDto.builder()
                .notCrashed(addCarViewModel.isNotCrashed())
                .engineCapacity(addCarViewModel.getEngineCapacity())
                .mileage(addCarViewModel.getMileage())
                .ownersCount(addCarViewModel.getOwnersCount())
                .username(auth.getName())
                .year(addCarViewModel.getYear())
                .price(addCarViewModel.getPrice())
                .build();
        carService.addCar(addCarDto);
        return "redirect:/cars/index";
    }

    @GetMapping("/addRandomCars")
    public String addRandomCars() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        carService.addRandomCars(auth.getName());
        return "redirect:/cars/index";
    }
}
