package com.carPriceCalculator.cars.persistance;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface CarRepository extends PagingAndSortingRepository<Car, UUID> {
}
