package com.carPriceCalculator.cars.persistance;

import com.carPriceCalculator.users.persistance.User;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(schema="public", name="cars")
public class Car {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", nullable = false, updatable = false)
    private UUID id;

    @Column(name = "year", nullable = false)
    private int year;

    @Column(name = "not_crashed", nullable = false)
    private boolean notCrashed;

    @Column(name = "engine_capacity", nullable = false)
    private double engineCapacity;

    @Column(name = "mileage", nullable = false)
    private double mileage;

    @Column(name = "owners_count", nullable = false)
    private int ownersCount;

    @Column(name = "price", nullable = false)
    private double price;

    @Column(name = "creation_date", nullable = false)
    private OffsetDateTime creationDate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private User user;
}
