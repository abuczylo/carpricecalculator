package com.carPriceCalculator.roles.persistance;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface RoleRepository extends PagingAndSortingRepository<Role, UUID> {

    Role findByName(String name);
}
