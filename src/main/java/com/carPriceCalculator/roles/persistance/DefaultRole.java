package com.carPriceCalculator.roles.persistance;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DefaultRole {

    USER("User"),
    ADMIN("Admin");

    private String name;
}
