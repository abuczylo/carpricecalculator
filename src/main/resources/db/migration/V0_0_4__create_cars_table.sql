CREATE TABLE IF NOT EXISTS public.cars(
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    year INT NOT NULL,
    not_crashed BOOLEAN NOT NULL,
    engine_capacity NUMERIC(2, 1) NOT NULL,
    mileage NUMERIC(9, 2) NOT NULL,
    owners_count INT NOT NULL,
    price NUMERIC(9, 2) NOT NULL,
    creation_date TIMESTAMPTZ NOT NULL DEFAULT now(),
    user_id UUID NOT NULL REFERENCES public.users(id),
    CONSTRAINT cars_pk PRIMARY KEY (id)
);