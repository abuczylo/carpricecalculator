CREATE TABLE IF NOT EXISTS public.users(
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    CONSTRAINT users_pk PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.user_roles(
    user_id uuid NOT NULL REFERENCES public.users(id),
    role_id uuid NOT NULL REFERENCES public.roles(id),
    CONSTRAINT user_roles_pk PRIMARY KEY (user_id, role_id)
);

INSERT INTO public.users(id, email, password) VALUES('4ea519fc-630f-4b1a-b2aa-fb4073b3272b', 'a.buczylo@gmail.com', '{bcrypt}$2a$10$6M9E2nj1GU7hPBW8cNwb9.5I214ZIl41SmSc2Nufh6.Ud3dA9egAW'); --Password1234
INSERT INTO public.user_roles(user_id, role_id) VALUES('4ea519fc-630f-4b1a-b2aa-fb4073b3272b', '22e87917-9033-4840-b461-8fff32c3b957');
INSERT INTO public.user_roles(user_id, role_id) VALUES('4ea519fc-630f-4b1a-b2aa-fb4073b3272b', 'fb94250d-5d4e-45fe-9f51-ded1e4c60ace');