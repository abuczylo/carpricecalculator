CREATE TABLE IF NOT EXISTS public.roles(
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    name VARCHAR(100) NOT NULL,
    CONSTRAINT roles_pk PRIMARY KEY (id)
);

INSERT INTO public.roles(id, name) VALUES('22e87917-9033-4840-b461-8fff32c3b957', 'User');
INSERT INTO public.roles(id, name) VALUES('fb94250d-5d4e-45fe-9f51-ded1e4c60ace', 'Admin');